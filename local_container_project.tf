locals {
  container_projects = {
    container-php-cicd = merge(
      local.container_defaults,
      {
        name        = "container-php-cicd"
        description = "Container for PHP CI/CD"
        topics      = concat(local.container_topics, ["php"])
      }
    )

    container-imgptr = merge(
      local.container_defaults,
      {
        name                            = "container-imgptr"
        description                     = "Container for imgptr."
        topics                          = concat(local.container_topics)
        container_registry_access_level = "private"
      }
    )
  }
  container_defaults = {
    group_id         = module.group["container"].id
    group_key        = "container"
    avatar           = "files/avatar/container.png"
    packages_enabled = true
  }
  container_topics = ["oci", "container"]
}
