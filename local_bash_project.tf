locals {
  bash_projects = {
    rpi-coreos-installer : {
      name        = "rpi-coreos-installer"
      description = "Raspberry Pi Coreos Installer"
      topics      = concat(local.bash_topics, ["gitlab"]),
      group_id    = module.group["bash"].id
      group_key   = "bash"
    }
  }
  bash_topics = ["bash"]
}
