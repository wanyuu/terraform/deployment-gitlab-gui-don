locals {
  groups = {
    wanyuu = {
      group_id    = data.gitlab_group.parent.id
      path        = "wanyuu"
      description = "Main"
      access_tokens = {
        RootAccess = {
          access_level = "owner"
          #          expires_at   = formatdate("YYYY-MM-DD", timeadd(timestamp(), "17490h"))
          scopes = ["api", "read_api", "read_registry", "write_registry", "read_repository", "write_repository"]
        }
      }
      labels = local.default_labels
    }

    bash = {
      path                    = "bash"
      description             = "Bash projects"
      parent_id               = data.gitlab_group.parent.id
      project_creation_level  = "maintainer"
      request_access_enabled  = true
      subgroup_creation_level = "owner"
    }
    terraform = {
      path                    = "terraform"
      description             = "Terraform modules"
      parent_id               = data.gitlab_group.parent.id
      project_creation_level  = "maintainer"
      request_access_enabled  = true
      subgroup_creation_level = "owner"
    }
    container = {
      path                    = "container"
      description             = "Container OCI code."
      parent_id               = data.gitlab_group.parent.id
      project_creation_level  = "maintainer"
      request_access_enabled  = true
      subgroup_creation_level = "owner"
    }
    php = {
      path                    = "php"
      description             = "PHP code."
      parent_id               = data.gitlab_group.parent.id
      project_creation_level  = "maintainer"
      request_access_enabled  = true
      subgroup_creation_level = "owner"
    }
  }
}
