locals {
  php_projects = {
    rico-lib : {
      import_url  = "https://gitlab.com/gui-don/rico-lib.git"
      name        = "rico-lib"
      description = "A bunch of well-tested PHP utilities"
      topics      = concat(local.php_topics, ["utilities"]),
      group_id    = module.group["php"].id
      group_key   = "php"
      avatar      = "files/avatar/rico.png"

      badges = {
        1 = {
          link_url  = "https://packagist.org/packages/gui-don/rico-library"
          image_url = "https://img.shields.io/packagist/php-v/gui-don/rico-library?color=brightgreen"
        }
        2 = {
          link_url  = "https://gitlab.com/%%{project_path}/-/commits/%%{default_branch}"
          image_url = "https://gitlab.com/%%{project_path}/badges/%%{default_branch}/coverage.svg"
        }
        3 = {
          link_url  = "https://scrutinizer-ci.com/g/gui-don/%%{project_name}/?branch=%%{default_branch}"
          image_url = "https://scrutinizer-ci.com/g/gui-don/%%{project_name}/badges/quality-score.png"
        }
      }
    }
  }
  php_topics = ["php"]
}
