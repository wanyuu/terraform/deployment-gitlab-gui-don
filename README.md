# Terraform deployment for gui-don giltab

Contains projects, project defaults options, labels, etc.

## Usage

This repository uses gitlab-ci. It will automatically deploy changes when pushed to master. In addition, it will execute
plans on feature branches and on merge requests.

<!-- BEGIN_TF_DOCS -->
## Requirements

| Name | Version |
|------|---------|
| terraform | >= 1.3.0 |
| gitlab | ~> 17.0 |

## Providers

| Name | Version |
|------|---------|
| gitlab | ~> 17.0 |

## Modules

| Name | Source | Version |
|------|--------|---------|
| group | gitlab.com/wild-beavers/module-gitlab-group/gitlab | ~> 3 |
| project | gitlab.com/wild-beavers/module-gitlab-project/gitlab | ~> 5 |

## Resources

| Name | Type |
|------|------|
| [gitlab_current_user.current](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/data-sources/current_user) | data source |
| [gitlab_group.parent](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/data-sources/group) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| gitlab\_token | gitlab application token. [See how to create a personal access token on gitlab](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html). | `string` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| groups | n/a |
| projects | n/a |
<!-- END_TF_DOCS -->
