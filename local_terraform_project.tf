locals {
  terraform_projects = {
    deployment-gitlab-gui-don = merge(
      local.terraform_defaults,
      {
        name                        = "deployment-gitlab-gui-don"
        description                 = "Deployment project to setup gitlab repositories, settings and groups."
        topics                      = concat(local.terraform_deployment_topics, ["gitlab"]),
        avatar                      = "files/avatar/git.png"
        infrastructure_access_level = "enabled"
      }
    )

    deployment-aws-imgptr = merge(
      local.terraform_defaults,
      {
        name                        = "deployment-gitlab-gui-don"
        description                 = "Deployment project to setup gitlab repositories, settings and groups."
        topics                      = concat(local.terraform_deployment_topics, ["gitlab"]),
        infrastructure_access_level = "enabled"
      }
    )
  }
  terraform_topics            = ["terraform"]
  terraform_deployment_topics = concat(local.terraform_topics, ["deployment"])
  terraform_defaults = {
    group_id         = module.group["terraform"].id
    group_key        = "terraform"
    avatar           = "files/avatar/terraform.png"
    packages_enabled = true
  }
}
