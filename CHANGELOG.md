## 0.4.0

- feat: adds `container-imgptr`

## 0.3.3

- chore: pins project to 5

## 0.3.2

- fix: minimal approval is 1

## 0.3.1

- fix: any_approver rules

## 0.3.0

- feat: adds basic validation rules

## 0.2.2

- feat: disables emails for everything

## 0.2.1

- chore: updates ci variables

## 0.2.0

- feat: maintainer can push

## 0.1.5

- fix: pre-commit

## 0.1.4

- fix: attempts to fix terraform apply

## 0.1.3

- fix: `parent_id` should be null by default

## 0.1.2

- fix: attempts to fix terraform plan

## 0.1.1

- fix: some code fixes

## 0.1.0

- feat: new repo

## 0.0.0

- tech: initialise repository
